import { /* example as */ input } from './DEPENDENCIES/input';

let costs = [];
for (let i = Math.min(...input); i < Math.max(...input) + 1; i++) {
	let currCost = 0;
	for (const pos of input) {
		currCost += cost(Math.abs(pos - i));
	}
	costs.push(currCost);
}

function cost(n: number) {
	return Math.floor((1 / 2) * n * (n + 1));
}

console.log(Math.min(...costs));
