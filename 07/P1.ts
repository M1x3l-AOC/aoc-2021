import { /* example as */ input } from './DEPENDENCIES/input';

let costs = [];
for (let i = Math.min(...input); i < Math.max(...input) + 1; i++) {
	let currCost = 0;
	for (const pos of input) {
		currCost += Math.abs(pos - i);
	}
	costs.push(currCost);
}

console.log(Math.min(...costs));
