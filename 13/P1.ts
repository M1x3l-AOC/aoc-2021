import { /* example as */ input } from './DEPENDENCIES/input';
import { Grid } from './DEPENDENCIES/util';

const [rawPoints, rawAxes] = input.split(/\n\n/);

const points = rawPoints
	.split(/\n/g)
	.map((line) => line.split(/,/g).map((num) => parseInt(num)));

const axes = rawAxes
	.split(/\n/g)
	.map((line) => line.match(/fold along (x|y)=(\d+)/)!.slice(1, 3))
	.map(
		(axis) =>
			({ AXIS: axis[0], num: parseInt(axis[1]) } as {
				AXIS: 'x' | 'y';
				num: number;
			})
	);

let grid = new Grid();

for (const point of points) {
	grid.set(point[1], point[0]);
}

for (const axis of axes) {
	const [g1, g2] = grid.split(axis.AXIS as 'x' | 'y', axis.num)!;

	let gOut: boolean[][] = [];

	for (let y = 0; y < g1.length; y++)
		for (let x = 0; x < g1[y].length; x++) {
			if (!gOut[y]) gOut[y] = [];
			if (axis.AXIS == 'y')
				gOut[y][x] = g1?.[y]?.[x] || g2?.[g2.length - y - 1]?.[x];
			else gOut[y][x] = g1?.[y]?.[x] || g2?.[y]?.[g2?.[y]?.length - x - 1];
		}

	let dotCount = 0;
	gOut.forEach((row) =>
		row.forEach((cell) => {
			if (cell) dotCount++;
		})
	);
	console.log(dotCount);

	grid = new Grid(gOut);
	process.exit();
}
