export class Grid {
	private grid: boolean[][] = [];

	constructor(grid?: boolean[][]) {
		if (grid) this.grid = grid;
	}

	getGrid = () => {
		let len = 0;
		this.grid.forEach((row) => (len = Math.max(row.length, len)));
		return Array.from(this.grid, (row) => [
			...(row || []),
			...Array(len - (row || []).length).fill(false),
		]);
	};

	at = (row: number, col: number) => this.grid[row][col];

	set(row: number, col: number, value = true) {
		if (!this.grid[row]) this.grid[row] = [];

		this.grid[row][col] = value;
		return this;
	}

	split = (axis: 'x' | 'y', num: number): [boolean[][], boolean[][]] => {
		const grid = this.getGrid();
		if (axis == 'y') {
			return [grid.slice(0, num), grid.slice(num + 1, grid.length + 1)] as [
				boolean[][],
				boolean[][]
			];
		} else {
			let h1: boolean[][] = [];
			let h2: boolean[][] = [];

			for (let y = 0; y < grid.length; y++)
				for (let x = 0; x < grid[y].length; x++) {
					if (x < grid[y].length >> 1) {
						if (!h1[y]) h1[y] = [];
						h1[y][x] = grid[y][x];
					} else {
						if (!h2[y]) h2[y] = [];
						h2[y][x] = grid[y][x];
					}
				}

			return [h1, h2];
		}
	};

	show() {
		let len = 0;

		this.grid.forEach((row) => (len = Math.max(row.length, len)));

		console.log(
			Array.from(this.grid, (row) =>
				Array.from(
					[...(row || []), ...Array(len - (row || []).length).fill(false)],
					(cell) => (cell ? '#' : '.')
				).join('')
			).join('\n')
		);
	}
}
