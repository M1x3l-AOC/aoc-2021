import { /* example as */ input } from './DEPENDENCIES/input';

let data = {
	0: input.filter((e) => e == 0).length,
	1: input.filter((e) => e == 1).length,
	2: input.filter((e) => e == 2).length,
	3: input.filter((e) => e == 3).length,
	4: input.filter((e) => e == 4).length,
	5: input.filter((e) => e == 5).length,
	6: input.filter((e) => e == 6).length,
	7: input.filter((e) => e == 7).length,
	8: input.filter((e) => e == 8).length,
};

for (let i = 0; i < 256; i++) {
	const {
		0: n0,
		1: n1,
		2: n2,
		3: n3,
		4: n4,
		5: n5,
		6: n6,
		7: n7,
		8: n8,
	} = { ...data };

	data[0] = n1;
	data[1] = n2;
	data[2] = n3;
	data[3] = n4;
	data[4] = n5;
	data[5] = n6;
	data[6] = n0 + n7;
	data[7] = n8;
	data[8] = n0;
}

let sum = 0;

for (let i = 0; i <= 8; i++) {
	sum += data[i as 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8] as number;
}

console.log(sum);
