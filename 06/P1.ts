import { example as input } from './DEPENDENCIES/input';

let inp = [...input];

for (let i = 0; i < 80; i++) {
	let tmp: number[] = [];
	inp = inp.map((num) => {
		if (num == 0) {
			tmp.push(8);
			return 6;
		}
		return num - 1;
	});
	inp.push(...tmp);
}

console.log(inp.length);
