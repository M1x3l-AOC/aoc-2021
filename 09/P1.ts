import { /* example as */ input } from './DEPENDENCIES/input';

const array = input.map((row) => row.split('').map((cell) => parseInt(cell)));

let sum = 0;
for (let y = 0; y < array.length; y++)
	for (let x = 0; x < array[0].length; x++)
		if (
			![
				array[y - 1]?.[x],
				array[y]?.[x - 1],
				array[y + 1]?.[x],
				array[y]?.[x + 1],
			].some((val) => val <= array[y][x])
		)
			sum += array[y][x] + 1;

console.log(sum);
