import { /* example as */ input } from './DEPENDENCIES/input';

const array = input.map((row) =>
	row.split('').map((cell) => ({ num: parseInt(cell), checked: false }))
);

let basins = [];

for (let y = 0; y < array.length; y++)
	for (let x = 0; x < array[0].length; x++)
		if (!getNeighbours(array, x, y).some((val) => val.num < array[y][x].num))
			basins.push(traverseBasin(array, x, y));

basins.sort((a, b) => a - b);

const largestBasins = [basins.pop()!, basins.pop()!, basins.pop()!];

console.log(largestBasins.reduce((acc, curr) => acc * curr));

///////////////
// Functions //
///////////////

function traverseBasin(
	array: { num: number; checked: boolean }[][],
	x: number,
	y: number
) {
	let currentSize = 1;
	if (array[y][x].checked) return 0;
	array[y][x].checked = true;
	if (array[y][x].num == 9) return 0;
	const neighbors = getNeighbours(array, x, y);

	for (const { x, y } of neighbors) {
		currentSize += traverseBasin(array, x, y) || 0;
	}

	return currentSize;
}

function getNeighbours(
	array: { num: number; checked: boolean }[][],
	x: number,
	y: number
) {
	return [
		{ x: x, y: y - 1, num: array[y - 1]?.[x]?.num },
		{ x: x - 1, y: y, num: array[y]?.[x - 1]?.num },
		{ x: x, y: y + 1, num: array[y + 1]?.[x]?.num },
		{ x: x + 1, y: y, num: array[y]?.[x + 1]?.num },
	].filter((elem) => elem.num !== undefined);
}
