import { /* example as */ input } from './DEPENDENCIES/input';

let string = '';
for (const row of input) {
	let rowStr = '';
	for (const char of row) rowStr += colored(parseInt(char));
	string += `${rowStr}\n`;
}

console.log(string);

function colored(num: number) {
	if (num == 9) return `\u001b[48;5;229m ${num}\u001b[0m`;
	return `\u001b[48;5;${236 + num * 2}m ${num}\u001b[0m`;
}
