import { /* example as */ input } from './DEPENDENCIES/input';
import { inRange, parseInput } from './DEPENDENCIES/util';

const range = parseInput(input);

let yPositions = [];

for (let x = 0; x < 1000; x++)
	for (let y = 0; y < 1000; y++) {
		let velocity: [number, number] = [x, y];
		let position: [number, number] = [0, 0];
		let maxY = 0;

		let hit = false;

		for (let i = 0; i < 1000 && !hit; i++) {
			position[0] += velocity[0];
			position[1] += velocity[1];

			velocity[0] += velocity[0] > 0 ? -1 : velocity[0] < 0 ? 1 : 0;
			velocity[1]--;

			maxY = Math.max(maxY, position[1]);
			hit = inRange(range, position);
		}

		if (hit) yPositions.push(maxY);
	}

console.log(Math.max(...yPositions));
