export const parseInput = (str: string) =>
	str
		.match(/target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)/)!
		.slice(1)
		.map((num) => parseInt(num)) as [number, number, number, number];

export function inRange(
	[x1, x2, y1, y2]: [number, number, number, number],
	[x, y]: [number, number]
) {
	return x1 <= x && x <= x2 && y1 <= y && y <= y2;
}
