import { /* example2_7 as */ input } from './DEPENDENCIES/input';
import { hexToBin, parsePacket } from './DEPENDENCIES/util';

console.log(parsePacket(hexToBin(input)).value);
