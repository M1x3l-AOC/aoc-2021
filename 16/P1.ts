import { /* example1_5 as */ input } from './DEPENDENCIES/input';
import { hexToBin, parsePacket } from './DEPENDENCIES/util';

console.log(parsePacket(hexToBin(input)).accumulativeVersionNumber);
