export enum Operators {
	SUM,
	PRODUCT,
	MINIMUM,
	MAXIMUM,
	LITERAL,
	GREATER_THAN,
	LESS_THAN,
	EQUAL_TO,
}

export interface Packet {
	version: number;
	accumulativeVersionNumber: number;
	type: Operators;
	value: number;
	bitRest: string;
}

export function evaluate(operation: Operators, subPackets: Packet[]): number {
	switch (operation) {
		case Operators.SUM: {
			return subPackets.reduce((acc, cur) => ({
				...acc,
				value: acc.value + cur.value,
			})).value;
		}
		case Operators.PRODUCT: {
			return subPackets.reduce((acc, cur) => ({
				...acc,
				value: acc.value * cur.value,
			})).value;
		}
		case Operators.MINIMUM: {
			return subPackets.reduce((acc, cur) => ({
				...acc,
				value: Math.min(acc.value, cur.value),
			})).value;
		}
		case Operators.MAXIMUM: {
			return subPackets.reduce((acc, cur) => ({
				...acc,
				value: Math.max(acc.value, cur.value),
			})).value;
		}
		case Operators.LITERAL: {
			return subPackets[0].value;
		}
		case Operators.GREATER_THAN: {
			return subPackets[0].value > subPackets[1].value ? 1 : 0;
		}
		case Operators.LESS_THAN: {
			return subPackets[0].value < subPackets[1].value ? 1 : 0;
		}
		case Operators.EQUAL_TO: {
			return subPackets[0].value == subPackets[1].value ? 1 : 0;
		}
		default:
			throw Error(`OPERATION '${Operators[operation]}' NOT IMPLEMENTED`);
	}
}

export function hexToBin(string: string): string {
	function replacer(match: string) {
		return (
			// prettier-ignore
			{ 
			'0': '0000', '1': '0001', '2': '0010', '3': '0011',
			'4': '0100', '5': '0101', '6': '0110', '7': '0111',
			'8': '1000', '9': '1001', 'a': '1010', 'b': '1011',
			'c': '1100', 'd': '1101', 'e': '1110', 'f': '1111',
		}[
			match.toLowerCase() as
			'0' | '1' | '2' | '3' |
			'4' | '5' | '6' | '7' |
			'8' | '9' | 'a' | 'b' |
			'c' | 'd' | 'e' | 'f'
		]!
		);
	}
	return string.replace(/(.)/g, replacer);
}

export function parsePacket(str: string): Packet {
	const [packetVersionBin, packetTypeBin] = str
		.match(/^(.{3})(.{3})(.*)/)!
		.slice(1);
	let unconsumed = str.slice(6);

	const [packetVersion, packetType] = [
		parseInt(packetVersionBin, 2),
		parseInt(packetTypeBin, 2),
	];

	let accumulativeVersionNumber = 0;
	let value = 0;
	let subPackets: Packet[] = [];
	if (packetType == Operators.LITERAL) {
		const groups = unconsumed.match(/(.{5})/g)!.map((num) => ({
			continue: new Boolean(parseInt(num[0])).valueOf(),
			bits: num.slice(1),
		}));

		let num = '';

		for (const group of groups) {
			num += group.bits;
			if (!group.continue) break;
		}
		unconsumed = unconsumed.slice((num.length / 4) * 5);
		value = parseInt(num, 2);
	} else {
		const lengthID = parseInt(unconsumed.match(/^(.)/)![1]);
		unconsumed = unconsumed.slice(1);

		if (lengthID == 1) {
			const length = parseInt(unconsumed.match(/(.{11})/)![1], 2);
			unconsumed = unconsumed.slice(11);

			let packetsRead = 0;
			while (packetsRead < length) {
				const packet = parsePacket(unconsumed);
				console.log(packet);
				subPackets.push(packet);
				unconsumed = packet.bitRest;
				packetsRead++;
				accumulativeVersionNumber += packet.accumulativeVersionNumber;
			}
		} else {
			const length = parseInt(unconsumed.match(/(.{15})/)![1], 2);
			unconsumed = unconsumed.slice(15);

			let readBitsLength = 0;
			while (readBitsLength < length) {
				const packet = parsePacket(unconsumed);
				console.log(packet);
				subPackets.push(packet);

				readBitsLength += unconsumed.length - packet.bitRest.length;
				unconsumed = unconsumed.slice(
					unconsumed.length - packet.bitRest.length
				);
				accumulativeVersionNumber += packet.accumulativeVersionNumber;
			}
		}
	}
	if (subPackets.length) value = evaluate(packetType, subPackets);

	return {
		version: packetVersion,
		accumulativeVersionNumber: accumulativeVersionNumber + packetVersion,
		type: packetType,
		value,
		bitRest: unconsumed,
	};
}
