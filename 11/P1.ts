import { /* example as */ input } from './DEPENDENCIES/input';
const ITERATIONS = 100;

interface Cell {
	num: number;
	flashed: boolean;
}

let array = input.map((row) =>
	row.map((cell) => ({ num: cell, flashed: false } as Cell))
);

let totalFlashes = 0;

for (let i = 0; i < ITERATIONS; i++) {
	array = array.map((row) =>
		row.map((cell) => ({ flashed: false, num: cell.num + 1 } as Cell))
	);
	for (let y = 0; y < array.length; y++)
		for (let x = 0; x < array[0].length; x++) flash(array, x, y);

	array = array.map((row) =>
		row.map((cell) => ({
			flashed: cell.num > 9 ? true : false,
			num: cell.num > 9 ? 0 : cell.num,
		}))
	);

	array.forEach((row) =>
		row.filter((cell) => cell.flashed).forEach(() => totalFlashes++)
	);
}

function flash(array: Cell[][], x: number, y: number) {
	if (array[y][x].num > 9 && !array[y][x].flashed) {
		array[y][x].flashed = true;

		for (const cell of getAdjacent(array, x, y)) {
			array[cell.y][cell.x].num++;
			flash(array, cell.x, cell.y);
		}
	}
}

console.log(totalFlashes);

function getAdjacent(
	array: Cell[][],
	x: number,
	y: number
): { x: number; y: number; num: number }[] {
	const offsets = [
		[-1, -1],
		[0, -1],
		[1, -1],
		[-1, 0],
		[1, 0],
		[-1, 1],
		[0, 1],
		[1, 1],
	];

	let neighbors = [];
	for (const offset of offsets) {
		neighbors.push({
			x: x - offset[0],
			y: y - offset[1],
			num: array[y - offset[1]]?.[x - offset[0]]?.num,
		});
	}

	return neighbors.filter((cell) => cell.num != undefined);
}

function show(array: Cell[][]) {
	console.log(
		array
			.map((row) =>
				row
					.map(
						(cell) =>
							`${cell.flashed ? `\u001b[48;5;22m` : `\u001b[48;5;88m`}${cell.num
								.toString()
								.padStart(2)}\u001b[0m`
					)
					.join('')
			)
			.join('\n')
	);
}
