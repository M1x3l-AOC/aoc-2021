export const example = `5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`
	.split(/\n/g)
	.map((row) => row.split('').map((char) => parseInt(char)));

export const example2 = `11111
19991
19191
19991
11111`
	.split(/\n/g)
	.map((row) => row.split('').map((char) => parseInt(char)));
export const input = `1443582148
6553734851
1451741246
8835218864
1662317262
1731656623
1128178367
5842351665
6677326843
7381433267`
	.split(/\n/g)
	.map((row) => row.split('').map((char) => parseInt(char)));
