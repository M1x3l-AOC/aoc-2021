import { input } from './DEPENDENCIES/input';

let aim = 0;
let pos = 0;
let depth = 0;

for (let str of input) {
	const [_, direction, amount] = str.match(/(\w+) (\d+)/)!;
	switch (direction) {
		case 'forward':
			pos += parseInt(amount);
			depth += aim * parseInt(amount);
			break;
		case 'up':
			aim -= parseInt(amount);
			break;
		case 'down':
			aim += parseInt(amount);
			break;
	}
}

console.log(depth * pos);
