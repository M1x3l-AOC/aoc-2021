import { input } from './DEPENDENCIES/input';

let pos = 0;
let depth = 0;

for (let str of input) {
	const [_, direction, amount] = str.match(/(\w+) (\d+)/)!;
	switch (direction) {
		case 'forward':
			pos += parseInt(amount);
			break;
		case 'up':
			depth -= parseInt(amount);
			break;
		case 'down':
			depth += parseInt(amount);
			break;
	}
}

console.log(depth * pos);
