import { input } from './DEPENDENCIES/input';

function rating(type: '1' | '0') {
	let out: string[] = [];
	let prevOut = [...input];
	for (let j = 0; j < prevOut[0].length; j++) {
		if (prevOut.length == 1) return prevOut[0];

		let digs: string[] = [];
		for (let i = 0; i < prevOut.length; i++) {
			digs.push(prevOut[i][j]);
		}

		for (let i = 0; i < prevOut.length; i++) {
			if (
				prevOut[i][j] ==
				(amount(digs, type) == 'EQUAL' ? type : amount(digs, type))
			)
				out.push(prevOut[i]);
		}
		prevOut = [...out];
		out = [];
	}
	return prevOut[0];
}

function amount(data: string[], type: '0' | '1') {
	const all = data.join('');
	const zeros = all.replace(/[^0]/g, '');
	const ones = all.replace(/[^1]/g, '');
	if (zeros.length == ones.length) return 'EQUAL';
	if (type == '1') {
		if (zeros.length > ones.length) return '0';
		else if (zeros.length < ones.length) return '1';
	} else if (type == '0') {
		if (zeros.length < ones.length) return '0';
		else if (zeros.length > ones.length) return '1';
	}
}
// Type 1 = oxi
// Type 0 = co2
console.log(parseInt(rating('1'), 2) * parseInt(rating('0'), 2));
