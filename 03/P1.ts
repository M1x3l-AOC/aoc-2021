import { input } from './DEPENDENCIES/input';

let digits = '';

for (let j = 0; j < input[0].length; j++) {
	let n: number[] = [0, 0];
	for (let i = 0; i < input.length; i++) {
		n[parseInt(input[i][j])]++;
	}
	digits += n[0] > n[1] ? '0' : '1';
}

const gammaRate = parseInt(digits, 2);
const epsilonRate = parseInt(
	digits.replace(/0/g, '2').replace(/1/g, '0').replace(/2/g, '1'),
	2
);

console.log(gammaRate * epsilonRate);
