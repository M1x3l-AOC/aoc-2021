import { /* example as */ input } from './DEPENDENCIES/input';

console.log(
	input
		.map(
			(line) =>
				line[1].filter(
					(str) =>
						str.length == 2 ||
						str.length == 4 ||
						str.length == 3 ||
						str.length == 7
				).length
		)
		.reduce((acc, curr) => acc + curr)
);
