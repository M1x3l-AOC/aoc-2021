import { /* example as */ input } from './DEPENDENCIES/input';

const values = input
	.map((line) =>
		line.map((part) => part.map((str) => str.split('').sort().join('')))
	)
	.map((line) => ({
		signalPatterns: line[0],
		outputValue: line[1],
	}));

let outputValues: number[] = [];

for (let i = 0; i < values.length; i++) {
	const line = values[i];
	const unified = [...line.signalPatterns, ...line.outputValue];
	let map: string[] = [];
	map[1] = strSort(unified.filter((str) => str.length == 2)[0]);
	map[4] = strSort(unified.filter((str) => str.length == 4)[0]);
	map[7] = strSort(unified.filter((str) => str.length == 3)[0]);
	map[8] = strSort(unified.filter((str) => str.length == 7)[0]);

	const poss235 = unified
		.filter((str) => str.length == 5)
		.map((str) => new Set(str));
	const poss069 = unified
		.filter((str) => str.length == 6)
		.map((str) => new Set(str));

	let sets: Set<string>[] = [];
	sets[1] = new Set(map[1]);
	sets[4] = new Set(map[4]);

	map[6] = Array.from(
		poss069.find((elem) => setIntersect(elem, sets[1]).length == 1)!
	).join('');
	const poss09 = poss069.filter((elem) => Array.from(elem).join('') != map[6]);
	map[9] = Array.from(
		poss09.find((elem) => setIntersect(new Set(elem), sets[4]).length == 4)!
	).join('');
	map[0] = poss09
		.map((elem) => Array.from(elem).join(''))
		.filter((elem) => elem != map[9])[0];

	map[3] = Array.from(
		poss235.find((elem) => setIntersect(elem, sets[1]).length == 2)!
	).join('');
	const poss25 = poss235.filter((elem) => Array.from(elem).join('') != map[3]);
	map[2] = Array.from(
		poss25.find((elem) => setIntersect(new Set(elem), sets[4]).length == 2)!
	).join('');
	map[5] = poss25
		.map((elem) => Array.from(elem).join(''))
		.filter((elem) => elem != map[2])[0];

	const segmentMap = new Map(
		Array.from(map.map((elem) => strSort(elem)).entries()).map((elem) =>
			elem.reverse()
		) as [string, number][]
	);

	let currentOutput = [];
	for (const val of line.outputValue) {
		currentOutput.push(segmentMap.get(val));
	}

	outputValues.push(parseInt(currentOutput.join('')));
}

console.log(outputValues.reduce((acc, curr) => acc + curr));

function strSort(str: string) {
	return str.split('').sort().join('');
}

function setIntersect(...[set1, set2]: [Set<string>, Set<string>]) {
	return Array.from(set1).filter((str) => set2.has(str));
}

function setDifference(...[set1, set2]: [Set<string>, Set<string>]) {
	return Array.from(set1).filter((str) => !set2.has(str));
}
