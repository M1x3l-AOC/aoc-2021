import { /* example as */ input } from './DEPENDENCIES/input';
import { CoordinateSystem } from './DEPENDENCIES/util';

const array = Array.from(
	new CoordinateSystem(input, 1).board,
	(row) => row || []
).map((row) => Array.from(row, (cell) => cell || 0));

const output = array
	.map((row) => row.filter((cell) => cell >= 2))
	.map((row) => row.length)
	.reduce((acc, curr) => acc + curr);
console.log('---');

console.log(output);
