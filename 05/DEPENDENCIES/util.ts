export class CoordinateSystem {
	readonly board: number[][] = [];
	private part: 1 | 2;
	constructor(coordinates: number[][][], part: 1 | 2) {
		this.part = part;
		for (const coords of coordinates) {
			for (const point of this.coordsToPointsOnLine(coords)) {
				const [x, y] = point;
				if (!this.board[y]) this.board[y] = [];
				if (!this.board[y][x]) this.board[y][x] = 0;
				this.board[y][x]++;
			}
		}
	}

	private coordsToPointsOnLine(coords: number[][]) {
		const [[x1, y1], [x2, y2]] = coords;

		let points = [];

		if (x1 == x2) {
			if (y1 > y2) for (let i = y1; i >= y2; i--) points.push([x1, i]);
			else if (y1 < y2) for (let i = y1; i <= y2; i++) points.push([x1, i]);
			else throw Error('y1 == y2');
		} else if (y1 == y2) {
			if (x1 > x2) for (let i = x1; i >= x2; i--) points.push([i, y1]);
			else if (x1 < x2) for (let i = x1; i <= x2; i++) points.push([i, y1]);
			else throw Error('x1 == x2');
		} else {
			if (this.part == 2) {
				for (let offset = 0; offset <= Math.abs(x2 - x1); offset++)
					if (x1 > x2 && y1 > y2) points.push([x1 - offset, y1 - offset]);
					else if (x1 < x2 && y1 > y2) points.push([x1 + offset, y1 - offset]);
					else if (x1 > x2 && y1 < y2) points.push([x1 - offset, y1 + offset]);
					else if (x1 < x2 && y1 < y2) points.push([x1 + offset, y1 + offset]);
					else throw Error(`${x1},${y1} -> ${x2},${y2} wtf?`);
			}
		}
		return points;
	}
}
