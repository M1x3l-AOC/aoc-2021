import { /* example as */ input } from './DEPENDENCIES/input';
import {
	mapToCharacters,
	mapToType,
	opening,
	Parenthesis,
	Stack,
	TypeEnum,
} from './DEPENDENCIES/util';

const array = input.map((row) =>
	row.map(
		(char) => ({ type: mapToType(char), opening: opening(char) } as Parenthesis)
	)
);

let incompleteRows = [];
for (const row of array) {
	const openingStack = new Stack();
	loop: {
		for (const currPar of row) {
			if (currPar.opening) openingStack.push(currPar);
			else if (currPar.type != openingStack.pop()?.type) break loop;
		}
		incompleteRows.push(row);
	}
}

let scores = [];
for (const row of incompleteRows) {
	const stack = new Stack();
	for (const currPar of row) {
		if (currPar.opening) stack.push(currPar);
		else stack.remove(currPar.type);
	}

	let autocomplete = '';
	for (let i = 0; stack.length(); i++) {
		autocomplete += mapToCharacters(stack.pop()!.type)[1];
	}
	scores.push(
		autocomplete
			.split('')
			.map((e) => TypeEnum[e as ')' | ']' | '}' | '>'] as number)
			.reduce((acc, curr) => acc * 5 + curr)
	);
}

scores.sort((a, b) => a - b);

console.log(scores[scores.length >> 1]);
