import { /* example as */ input } from './DEPENDENCIES/input';
import { mapToType, opening, Parenthesis, Stack } from './DEPENDENCIES/util';

const array = input.map((row) =>
	row.map(
		(char) => ({ type: mapToType(char), opening: opening(char) } as Parenthesis)
	)
);

let errorScore = 0;

for (const row of array) {
	let currentErrorScore = 0;
	const stack = new Stack();

	charLoop: for (const currentParenthesis of row) {
		if (currentParenthesis.opening) stack.push(currentParenthesis);
		else {
			const topElement = stack.pop();
			if (topElement?.type != currentParenthesis?.type) {
				currentErrorScore +=
					currentParenthesis.type == 'NORMAL'
						? 3
						: currentParenthesis.type == 'SQUARE'
						? 57
						: currentParenthesis.type == 'CURLY'
						? 1197
						: 25137;
				break charLoop;
			}
		}
	}

	errorScore += currentErrorScore;
}

console.log(errorScore);
