export type Type = 'SQUARE' | 'NORMAL' | 'CURLY' | 'ANGLED';
export enum TypeEnum {
	'_',
	')',
	']',
	'}',
	'>',
}

export interface Parenthesis {
	type: Type;
	opening: boolean;
}

export class Stack {
	private stack: Parenthesis[] = [];
	constructor() {}

	push = (elem: Parenthesis) => this.stack.push(elem);
	pushFront = (elem: Parenthesis) => this.stack.unshift(elem);
	pop = () => this.stack.pop();
	popFront = () => this.stack.shift();
	top = () => this.stack[this.stack.length - 1];
	length = (len?: number, type?: 'EQ' | 'LT' | 'LE' | 'GT' | 'GE') => {
		if (len)
			switch (type) {
				case 'EQ':
					return this.stack.length == len;
				case 'LT':
					return this.stack.length < len;
				case 'LE':
					this.stack.length <= len;
				case 'GT':
					return this.stack.length > len;
				case 'GE':
					return this.stack.length >= len;
			}
		else return this.stack.length;
	};
	remove = (type: Type) => {
		const idx = this.stack.map((e) => e.type).lastIndexOf(type);
		const elem = this.stack[idx];
		this.stack = this.stack.filter((_, i) => i != idx);
		return elem;
	};
}

export function mapToType(str: string): Type {
	if (['[', ']'].includes(str)) return 'SQUARE';
	else if (['(', ')'].includes(str)) return 'NORMAL';
	else if (['{', '}'].includes(str)) return 'CURLY';
	else if (['<', '>'].includes(str)) return 'ANGLED';
	else throw Error(`unknown Parenthesis type \`${str}\``);
}

export function mapToCharacters(type: Type): string[] {
	switch (type) {
		case 'ANGLED':
			return ['<', '>'];
		case 'CURLY':
			return ['{', '}'];
		case 'NORMAL':
			return ['(', ')'];
		case 'SQUARE':
			return ['[', ']'];
	}
}

export function opening(str: string): boolean {
	if (['{', '(', '[', '<'].includes(str)) return true;
	if (['}', ')', ']', '>'].includes(str)) return false;
	else throw Error(`unknown Parenthesis type \`${str}\``);
}
