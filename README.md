# Advent of Code – 2021

To see, what this is about, you can visit [Advent of Code's about page](https://adventofcode.com/2021/about)

## TOC

- [Advent of Code – 2021](#advent-of-code--2021)
	- [TOC](#toc)
	- [Structure](#structure)
	- [sh-files](#sh-files)
		- [Main run file: `run.sh`](#main-run-file-runsh)
		- [Visualising file: `visualise.sh`](#visualising-file-visualisesh)

## Structure

All files will be located in their corresponding folder

The folder structure will look something like this:

```
root +- {day} + PROBLEM        (file) [ description from the website                 ]
     |        + P1.ts          (file) [ code to solve part one                       ]
     |        + P2.ts          (file) [ code to solve part two                       ]
     |        +?V.ts           (file) [ optional file used to visualise the problem  ]
     |        + DEPENDENCIES   ( dir) [ code, that may be needed to run the solution ]
     |
     +- node_modules           ( dir) [ node.js's modules folder                     ]
     |
     +- README.md              (file) [ this README file                             ]
```

## sh-files

### Main run file: `run.sh`

```console
./run.sh <day> <part>

<day>  : 01 - 25 (only works up to implemented day)
<part> : 1 / 2
```

### Visualising file: `visualise.sh`

```console
./visualise.sh <day>

<day> : 01 - 25 (only works up to implemented day)
```
