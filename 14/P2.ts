import { /* example as */ input } from './DEPENDENCIES/input';

const [start, rawInstructions] = input.split(/\n\n/);

const instructions = rawInstructions
	.split(/\n/g)
	.map((line) => JSON.parse(`{ "${line.split(/ -> /).join('": "')}"}`))
	.reduce((acc, curr) => {
		return { ...acc, [Object.keys(curr)[0]]: Object.values(curr)[0] };
	});

let pairMap: any = {};

for (const pair of getPairs(start)) {
	if (!pairMap![pair]) pairMap[pair] = 0;
	pairMap[pair]++;
}

for (const _ of new Array(40).fill(null)) {
	let newPairMap: any = {};
	for (const [pair, count] of Object.entries(pairMap) as [string, number][]) {
		const insertion = instructions[pair];
		for (const p of [`${pair[0]}${insertion}`, `${insertion}${pair[1]}`]) {
			if (!newPairMap[p]) newPairMap[p] = 0;
			newPairMap[p] += count;
		}
	}
	pairMap = newPairMap;
}

let charMap: any = {};

for (const [pair, count] of Object.entries(pairMap) as [string, number][]) {
	if (!charMap![pair[1]]) charMap[pair[1]] = 0;
	charMap[pair[1]] += count;
}
charMap[start[0]]++;

const lengths = Object.values(charMap).sort(
	(a, b) => ((a as number) - (b as number)) as number
) as number[];

console.log(lengths[lengths.length - 1] - lengths[0]);

function getPairs(string: string) {
	let array = [];
	for (let i = 0; i < string.length - 1; i++)
		array.push(string[i] + string[i + 1]);
	return array;
}
