import { /* example as */ input } from './DEPENDENCIES/input';

const [start, rawInstructions] = input.split(/\n\n/);

const instructions = rawInstructions
	.split(/\n/g)
	.map((line) => JSON.parse(`{ "${line.split(/ -> /).join('": "')}"}`))
	.reduce((acc, curr) => {
		return { ...acc, [Object.keys(curr)[0]]: Object.values(curr)[0] };
	});

let str = start;

for (const _ of new Array(10).fill(null))
	str =
		str[0] +
		getPairs(str)
			.map((str) => `${instructions[str]}${str[1]}`)
			.join('');

let charMap: any = {};

for (const char of str) {
	if (!charMap![char]) charMap[char] = 0;
	charMap[char]++;
}

const lengths = Object.values(charMap).sort(
	(a, b) => ((a as number) - (b as number)) as number
) as number[];
console.log(lengths[lengths.length - 1] - lengths[0]);

function getPairs(string: string) {
	let array = [];
	for (let i = 0; i < string.length - 1; i++)
		array.push(string[i] + string[i + 1]);
	return array;
}
