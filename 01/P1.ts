import { input } from './DEPENDENCIES/input';

let acc = 0;
let prev = Number.MAX_VALUE;
for (let num of input) {
	if (prev < num) acc++;
	prev = num;
}

console.log(acc);
