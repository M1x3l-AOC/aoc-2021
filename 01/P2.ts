import { input } from './DEPENDENCIES/input';

let sums: number[] = [];

for (let i = 0; i < input.length - 2; i++) {
	sums.push(input[i] + input[i + 1] + input[i + 2]);
}

let acc = 0;
let prev = Number.MAX_VALUE;
for (let num of sums) {
	if (prev < num) acc++;
	prev = num;
}

console.log(acc);
