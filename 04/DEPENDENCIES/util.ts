export interface Cell {
	num: number;
	checked: boolean;
}

export class Board {
	id: number = 0;
	private board: Cell[];
	constructor(str: Cell[]) {
		this.board = str;
	}

	at(row: number, col: number) {
		if (row > 5 || col > 5) throw Error('Array out of bounds');
		return this.board[row * 5 + col];
	}

	check(row: number, col: number) {
		if (row > 5 || col > 5) throw Error('Array out of bounds');

		this.board[row * 5 + col].checked = true;
		return this;
	}

	find(num: number): [number, number] | null {
		for (let i = 0; i < this.board.length; i++) {
			const element = this.board[i];
			if (element.num == num) return [(i - (i % 5)) / 5, i % 5];
		}
		return null;
	}

	hasWon() {
		let globBingo: boolean = false;
		loop: for (let i = 0; i < 5; i++) {
			let cells: Cell[] = [];
			for (let j = 0; j < 5; j++) {
				cells.push(this.at(i, j));
			}
			globBingo = cells.every((cell) => cell.checked);
			if (globBingo) break loop;
		}

		if (!globBingo)
			loop: for (let i = 0; i < 5; i++) {
				let cells: Cell[] = [];
				for (let j = 0; j < 5; j++) {
					cells.push(this.at(j, i));
				}
				globBingo = cells.every((cell) => cell.checked);
				if (globBingo) break loop;
			}

		if (!globBingo) return false;
		return [this.board.filter((cell) => !cell.checked), this.id];
	}
}
