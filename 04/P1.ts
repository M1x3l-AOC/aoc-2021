import {
	/* example_boards as */ input_boards,
	/* example_numbers as */ input_numbers,
} from './DEPENDENCIES/input';
import { Cell } from './DEPENDENCIES/util';

for (const num of input_numbers) {
	let winBoard: Cell[] = [];

	for (const board of input_boards) {
		const idx = board.find(num);
		if (idx) board.check(...idx);
		const [won, _] = board.hasWon() as [Cell[], number];
		if (won) {
			winBoard = won;
		}
	}

	if (winBoard.length) {
		console.log(
			winBoard.map((cell) => cell.num).reduce((acc, curr) => acc + curr) * num
		);
		break;
	}
}
