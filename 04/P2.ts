import {
	/* example_boards as  */ input_boards,
	/* example_numbers as  */ input_numbers,
} from './DEPENDENCIES/input';
import { Cell } from './DEPENDENCIES/util';

let wonBoardIDs: number[] = [];

let wonBoards: number[] = [];

for (const num of input_numbers) {
	let winBoard: Cell[] = [];

	for (const board of input_boards) {
		const idx = board.find(num);
		if (idx) board.check(...idx);

		const [won, id] = board.hasWon() as [Cell[], number];
		if (won && !wonBoardIDs.includes(id)) {
			winBoard = won;
			wonBoardIDs.push(id);
		}
	}

	if (winBoard.length) {
		wonBoards.push(
			winBoard.map((cell) => cell.num).reduce((acc, curr) => acc + curr) * num
		);
	}
}

console.log(wonBoards[wonBoards.length - 1]);
