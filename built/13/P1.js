"use strict";
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
var _f = input_1.input.split(/\n\n/), rawPoints = _f[0], rawAxes = _f[1];
var points = rawPoints
    .split(/\n/g)
    .map(function (line) { return line.split(/,/g).map(function (num) { return parseInt(num); }); });
var axes = rawAxes
    .split(/\n/g)
    .map(function (line) { return line.match(/fold along (x|y)=(\d+)/).slice(1, 3); })
    .map(function (axis) {
    return ({ AXIS: axis[0], num: parseInt(axis[1]) });
});
var grid = new util_1.Grid();
for (var _i = 0, points_1 = points; _i < points_1.length; _i++) {
    var point = points_1[_i];
    grid.set(point[1], point[0]);
}
var _loop_1 = function (axis) {
    var _a = grid.split(axis.AXIS, axis.num), g1 = _a[0], g2 = _a[1];
    var gOut = [];
    for (var y = 0; y < g1.length; y++)
        for (var x = 0; x < g1[y].length; x++) {
            if (!gOut[y])
                gOut[y] = [];
            if (axis.AXIS == 'y')
                gOut[y][x] = ((_a = g1 === null || g1 === void 0 ? void 0 : g1[y]) === null || _a === void 0 ? void 0 : _a[x]) || ((_b = g2 === null || g2 === void 0 ? void 0 : g2[g2.length - y - 1]) === null || _b === void 0 ? void 0 : _b[x]);
            else
                gOut[y][x] = ((_c = g1 === null || g1 === void 0 ? void 0 : g1[y]) === null || _c === void 0 ? void 0 : _c[x]) || ((_d = g2 === null || g2 === void 0 ? void 0 : g2[y]) === null || _d === void 0 ? void 0 : _d[((_e = g2 === null || g2 === void 0 ? void 0 : g2[y]) === null || _e === void 0 ? void 0 : _e.length) - x - 1]);
        }
    var dotCount = 0;
    gOut.forEach(function (row) {
        return row.forEach(function (cell) {
            if (cell)
                dotCount++;
        });
    });
    console.log(dotCount);
    grid = new util_1.Grid(gOut);
    process.exit();
};
for (var _g = 0, axes_1 = axes; _g < axes_1.length; _g++) {
    var axis = axes_1[_g];
    _loop_1(axis);
}
