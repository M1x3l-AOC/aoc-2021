"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Grid = /** @class */ (function () {
    function Grid(grid) {
        var _this = this;
        this.grid = [];
        this.getGrid = function () {
            var len = 0;
            _this.grid.forEach(function (row) { return (len = Math.max(row.length, len)); });
            return Array.from(_this.grid, function (row) { return __spreadArrays((row || []), Array(len - (row || []).length).fill(false)); });
        };
        this.at = function (row, col) { return _this.grid[row][col]; };
        this.split = function (axis, num) {
            var grid = _this.getGrid();
            if (axis == 'y') {
                return [grid.slice(0, num), grid.slice(num + 1, grid.length + 1)];
            }
            else {
                var h1 = [];
                var h2 = [];
                for (var y = 0; y < grid.length; y++)
                    for (var x = 0; x < grid[y].length; x++) {
                        if (x < grid[y].length >> 1) {
                            if (!h1[y])
                                h1[y] = [];
                            h1[y][x] = grid[y][x];
                        }
                        else {
                            if (!h2[y])
                                h2[y] = [];
                            h2[y][x] = grid[y][x];
                        }
                    }
                return [h1, h2];
            }
        };
        if (grid)
            this.grid = grid;
    }
    Grid.prototype.set = function (row, col, value) {
        if (value === void 0) { value = true; }
        if (!this.grid[row])
            this.grid[row] = [];
        this.grid[row][col] = value;
        return this;
    };
    Grid.prototype.show = function () {
        var len = 0;
        this.grid.forEach(function (row) { return (len = Math.max(row.length, len)); });
        console.log(Array.from(this.grid, function (row) {
            return Array.from(__spreadArrays((row || []), Array(len - (row || []).length).fill(false)), function (cell) { return (cell ? '#' : '.'); }).join('');
        }).join('\n'));
    };
    return Grid;
}());
exports.Grid = Grid;
