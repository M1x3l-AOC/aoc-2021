"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
console.log(util_1.parsePacket(util_1.hexToBin(input_1.input)).accumulativeVersionNumber);
