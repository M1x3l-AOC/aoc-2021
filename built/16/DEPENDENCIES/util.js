"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Operators;
(function (Operators) {
    Operators[Operators["SUM"] = 0] = "SUM";
    Operators[Operators["PRODUCT"] = 1] = "PRODUCT";
    Operators[Operators["MINIMUM"] = 2] = "MINIMUM";
    Operators[Operators["MAXIMUM"] = 3] = "MAXIMUM";
    Operators[Operators["LITERAL"] = 4] = "LITERAL";
    Operators[Operators["GREATER_THAN"] = 5] = "GREATER_THAN";
    Operators[Operators["LESS_THAN"] = 6] = "LESS_THAN";
    Operators[Operators["EQUAL_TO"] = 7] = "EQUAL_TO";
})(Operators = exports.Operators || (exports.Operators = {}));
function evaluate(operation, subPackets) {
    switch (operation) {
        case Operators.SUM: {
            return subPackets.reduce(function (acc, cur) { return (__assign(__assign({}, acc), { value: acc.value + cur.value })); }).value;
        }
        case Operators.PRODUCT: {
            return subPackets.reduce(function (acc, cur) { return (__assign(__assign({}, acc), { value: acc.value * cur.value })); }).value;
        }
        case Operators.MINIMUM: {
            return subPackets.reduce(function (acc, cur) { return (__assign(__assign({}, acc), { value: Math.min(acc.value, cur.value) })); }).value;
        }
        case Operators.MAXIMUM: {
            return subPackets.reduce(function (acc, cur) { return (__assign(__assign({}, acc), { value: Math.max(acc.value, cur.value) })); }).value;
        }
        case Operators.LITERAL: {
            return subPackets[0].value;
        }
        case Operators.GREATER_THAN: {
            return subPackets[0].value > subPackets[1].value ? 1 : 0;
        }
        case Operators.LESS_THAN: {
            return subPackets[0].value < subPackets[1].value ? 1 : 0;
        }
        case Operators.EQUAL_TO: {
            return subPackets[0].value == subPackets[1].value ? 1 : 0;
        }
        default:
            throw Error("OPERATION '" + Operators[operation] + "' NOT IMPLEMENTED");
    }
}
exports.evaluate = evaluate;
function hexToBin(string) {
    function replacer(match) {
        return (
        // prettier-ignore
        {
            '0': '0000', '1': '0001', '2': '0010', '3': '0011',
            '4': '0100', '5': '0101', '6': '0110', '7': '0111',
            '8': '1000', '9': '1001', 'a': '1010', 'b': '1011',
            'c': '1100', 'd': '1101', 'e': '1110', 'f': '1111',
        }[match.toLowerCase()]);
    }
    return string.replace(/(.)/g, replacer);
}
exports.hexToBin = hexToBin;
function parsePacket(str) {
    var _a = str
        .match(/^(.{3})(.{3})(.*)/)
        .slice(1), packetVersionBin = _a[0], packetTypeBin = _a[1];
    var unconsumed = str.slice(6);
    var _b = [
        parseInt(packetVersionBin, 2),
        parseInt(packetTypeBin, 2),
    ], packetVersion = _b[0], packetType = _b[1];
    var accumulativeVersionNumber = 0;
    var value = 0;
    var subPackets = [];
    if (packetType == Operators.LITERAL) {
        var groups = unconsumed.match(/(.{5})/g).map(function (num) { return ({
            continue: new Boolean(parseInt(num[0])).valueOf(),
            bits: num.slice(1),
        }); });
        var num = '';
        for (var _i = 0, groups_1 = groups; _i < groups_1.length; _i++) {
            var group = groups_1[_i];
            num += group.bits;
            if (!group.continue)
                break;
        }
        unconsumed = unconsumed.slice((num.length / 4) * 5);
        value = parseInt(num, 2);
    }
    else {
        var lengthID = parseInt(unconsumed.match(/^(.)/)[1]);
        unconsumed = unconsumed.slice(1);
        if (lengthID == 1) {
            var length_1 = parseInt(unconsumed.match(/(.{11})/)[1], 2);
            unconsumed = unconsumed.slice(11);
            var packetsRead = 0;
            while (packetsRead < length_1) {
                var packet = parsePacket(unconsumed);
                console.log(packet);
                subPackets.push(packet);
                unconsumed = packet.bitRest;
                packetsRead++;
                accumulativeVersionNumber += packet.accumulativeVersionNumber;
            }
        }
        else {
            var length_2 = parseInt(unconsumed.match(/(.{15})/)[1], 2);
            unconsumed = unconsumed.slice(15);
            var readBitsLength = 0;
            while (readBitsLength < length_2) {
                var packet = parsePacket(unconsumed);
                console.log(packet);
                subPackets.push(packet);
                readBitsLength += unconsumed.length - packet.bitRest.length;
                unconsumed = unconsumed.slice(unconsumed.length - packet.bitRest.length);
                accumulativeVersionNumber += packet.accumulativeVersionNumber;
            }
        }
    }
    if (subPackets.length)
        value = evaluate(packetType, subPackets);
    return {
        version: packetVersion,
        accumulativeVersionNumber: accumulativeVersionNumber + packetVersion,
        type: packetType,
        value: value,
        bitRest: unconsumed,
    };
}
exports.parsePacket = parsePacket;
