"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CoordinateSystem = /** @class */ (function () {
    function CoordinateSystem(coordinates, part) {
        this.board = [];
        this.part = part;
        for (var _i = 0, coordinates_1 = coordinates; _i < coordinates_1.length; _i++) {
            var coords = coordinates_1[_i];
            for (var _a = 0, _b = this.coordsToPointsOnLine(coords); _a < _b.length; _a++) {
                var point = _b[_a];
                var x = point[0], y = point[1];
                if (!this.board[y])
                    this.board[y] = [];
                if (!this.board[y][x])
                    this.board[y][x] = 0;
                this.board[y][x]++;
            }
        }
    }
    CoordinateSystem.prototype.coordsToPointsOnLine = function (coords) {
        var _a = coords[0], x1 = _a[0], y1 = _a[1], _b = coords[1], x2 = _b[0], y2 = _b[1];
        var points = [];
        if (x1 == x2) {
            if (y1 > y2)
                for (var i = y1; i >= y2; i--)
                    points.push([x1, i]);
            else if (y1 < y2)
                for (var i = y1; i <= y2; i++)
                    points.push([x1, i]);
            else
                throw Error('y1 == y2');
        }
        else if (y1 == y2) {
            if (x1 > x2)
                for (var i = x1; i >= x2; i--)
                    points.push([i, y1]);
            else if (x1 < x2)
                for (var i = x1; i <= x2; i++)
                    points.push([i, y1]);
            else
                throw Error('x1 == x2');
        }
        else {
            if (this.part == 2) {
                for (var offset = 0; offset <= Math.abs(x2 - x1); offset++)
                    if (x1 > x2 && y1 > y2)
                        points.push([x1 - offset, y1 - offset]);
                    else if (x1 < x2 && y1 > y2)
                        points.push([x1 + offset, y1 - offset]);
                    else if (x1 > x2 && y1 < y2)
                        points.push([x1 - offset, y1 + offset]);
                    else if (x1 < x2 && y1 < y2)
                        points.push([x1 + offset, y1 + offset]);
                    else
                        throw Error(x1 + "," + y1 + " -> " + x2 + "," + y2 + " wtf?");
            }
        }
        return points;
    };
    return CoordinateSystem;
}());
exports.CoordinateSystem = CoordinateSystem;
