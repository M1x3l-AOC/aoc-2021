"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
var array = Array.from(new util_1.CoordinateSystem(input_1.input, 2).board, function (row) { return row || []; }).map(function (row) { return Array.from(row, function (cell) { return cell || 0; }); });
var output = array
    .map(function (row) { return row.filter(function (cell) { return cell >= 2; }); })
    .map(function (row) { return row.length; })
    .reduce(function (acc, curr) { return acc + curr; });
console.log('---');
console.log(output);
