"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var _a = input_1.input.split(/\n\n/), start = _a[0], rawInstructions = _a[1];
var instructions = rawInstructions
    .split(/\n/g)
    .map(function (line) { return JSON.parse("{ \"" + line.split(/ -> /).join('": "') + "\"}"); })
    .reduce(function (acc, curr) {
    var _a;
    return __assign(__assign({}, acc), (_a = {}, _a[Object.keys(curr)[0]] = Object.values(curr)[0], _a));
});
var str = start;
for (var _i = 0, _b = new Array(10).fill(null); _i < _b.length; _i++) {
    var _ = _b[_i];
    str =
        str[0] +
            getPairs(str)
                .map(function (str) { return "" + instructions[str] + str[1]; })
                .join('');
}
var charMap = {};
for (var _c = 0, str_1 = str; _c < str_1.length; _c++) {
    var char = str_1[_c];
    if (!charMap[char])
        charMap[char] = 0;
    charMap[char]++;
}
var lengths = Object.values(charMap).sort(function (a, b) { return (a - b); });
console.log(lengths[lengths.length - 1] - lengths[0]);
function getPairs(string) {
    var array = [];
    for (var i = 0; i < string.length - 1; i++)
        array.push(string[i] + string[i + 1]);
    return array;
}
