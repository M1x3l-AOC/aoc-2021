"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var _a = input_1.input.split(/\n\n/), start = _a[0], rawInstructions = _a[1];
var instructions = rawInstructions
    .split(/\n/g)
    .map(function (line) { return JSON.parse("{ \"" + line.split(/ -> /).join('": "') + "\"}"); })
    .reduce(function (acc, curr) {
    var _a;
    return __assign(__assign({}, acc), (_a = {}, _a[Object.keys(curr)[0]] = Object.values(curr)[0], _a));
});
var pairMap = {};
for (var _i = 0, _b = getPairs(start); _i < _b.length; _i++) {
    var pair = _b[_i];
    if (!pairMap[pair])
        pairMap[pair] = 0;
    pairMap[pair]++;
}
for (var _c = 0, _d = new Array(40).fill(null); _c < _d.length; _c++) {
    var _ = _d[_c];
    var newPairMap = {};
    for (var _e = 0, _f = Object.entries(pairMap); _e < _f.length; _e++) {
        var _g = _f[_e], pair = _g[0], count = _g[1];
        var insertion = instructions[pair];
        for (var _h = 0, _j = ["" + pair[0] + insertion, "" + insertion + pair[1]]; _h < _j.length; _h++) {
            var p = _j[_h];
            if (!newPairMap[p])
                newPairMap[p] = 0;
            newPairMap[p] += count;
        }
    }
    pairMap = newPairMap;
}
var charMap = {};
for (var _k = 0, _l = Object.entries(pairMap); _k < _l.length; _k++) {
    var _m = _l[_k], pair = _m[0], count = _m[1];
    if (!charMap[pair[1]])
        charMap[pair[1]] = 0;
    charMap[pair[1]] += count;
}
charMap[start[0]]++;
var lengths = Object.values(charMap).sort(function (a, b) { return (a - b); });
console.log(lengths[lengths.length - 1] - lengths[0]);
function getPairs(string) {
    var array = [];
    for (var i = 0; i < string.length - 1; i++)
        array.push(string[i] + string[i + 1]);
    return array;
}
