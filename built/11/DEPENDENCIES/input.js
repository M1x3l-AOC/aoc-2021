"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.example = "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526"
    .split(/\n/g)
    .map(function (row) { return row.split('').map(function (char) { return parseInt(char); }); });
exports.example2 = "11111\n19991\n19191\n19991\n11111"
    .split(/\n/g)
    .map(function (row) { return row.split('').map(function (char) { return parseInt(char); }); });
exports.input = "1443582148\n6553734851\n1451741246\n8835218864\n1662317262\n1731656623\n1128178367\n5842351665\n6677326843\n7381433267"
    .split(/\n/g)
    .map(function (row) { return row.split('').map(function (char) { return parseInt(char); }); });
