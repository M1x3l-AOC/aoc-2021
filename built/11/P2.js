"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var array = input_1.input.map(function (row) {
    return row.map(function (cell) { return ({ num: cell, flashed: false }); });
});
var totalFlashes = 0;
for (var i = 0; true; i++) {
    var flashDiff = totalFlashes;
    array = array.map(function (row) {
        return row.map(function (cell) { return ({ flashed: false, num: cell.num + 1 }); });
    });
    for (var y = 0; y < array.length; y++)
        for (var x = 0; x < array[0].length; x++)
            flash(array, x, y);
    array = array.map(function (row) {
        return row.map(function (cell) { return ({
            flashed: cell.num > 9 ? true : false,
            num: cell.num > 9 ? 0 : cell.num,
        }); });
    });
    array.forEach(function (row) {
        return row.filter(function (cell) { return cell.flashed; }).forEach(function () { return totalFlashes++; });
    });
    flashDiff -= totalFlashes;
    if (flashDiff == -100) {
        console.log("Iteration " + (i + 1));
        process.exit();
    }
}
function flash(array, x, y) {
    if (array[y][x].num > 9 && !array[y][x].flashed) {
        array[y][x].flashed = true;
        for (var _i = 0, _a = getAdjacent(array, x, y); _i < _a.length; _i++) {
            var cell = _a[_i];
            array[cell.y][cell.x].num++;
            flash(array, cell.x, cell.y);
        }
    }
}
console.log(totalFlashes);
function getAdjacent(array, x, y) {
    var _a, _b;
    var offsets = [
        [-1, -1],
        [0, -1],
        [1, -1],
        [-1, 0],
        [1, 0],
        [-1, 1],
        [0, 1],
        [1, 1],
    ];
    var neighbors = [];
    for (var _i = 0, offsets_1 = offsets; _i < offsets_1.length; _i++) {
        var offset = offsets_1[_i];
        neighbors.push({
            x: x - offset[0],
            y: y - offset[1],
            num: (_b = (_a = array[y - offset[1]]) === null || _a === void 0 ? void 0 : _a[x - offset[0]]) === null || _b === void 0 ? void 0 : _b.num,
        });
    }
    return neighbors.filter(function (cell) { return cell.num != undefined; });
}
function show(array) {
    console.log(array
        .map(function (row) {
        return row
            .map(function (cell) {
            return "" + (cell.flashed ? "\u001B[48;5;22m" : "\u001B[48;5;88m") + cell.num
                .toString()
                .padStart(2) + "\u001B[0m";
        })
            .join('');
    })
        .join('\n'));
}
