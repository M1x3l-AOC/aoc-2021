"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeEnum;
(function (TypeEnum) {
    TypeEnum[TypeEnum["_"] = 0] = "_";
    TypeEnum[TypeEnum[")"] = 1] = ")";
    TypeEnum[TypeEnum["]"] = 2] = "]";
    TypeEnum[TypeEnum["}"] = 3] = "}";
    TypeEnum[TypeEnum[">"] = 4] = ">";
})(TypeEnum = exports.TypeEnum || (exports.TypeEnum = {}));
var Stack = /** @class */ (function () {
    function Stack() {
        var _this = this;
        this.stack = [];
        this.push = function (elem) { return _this.stack.push(elem); };
        this.pushFront = function (elem) { return _this.stack.unshift(elem); };
        this.pop = function () { return _this.stack.pop(); };
        this.popFront = function () { return _this.stack.shift(); };
        this.top = function () { return _this.stack[_this.stack.length - 1]; };
        this.length = function (len, type) {
            if (len)
                switch (type) {
                    case 'EQ':
                        return _this.stack.length == len;
                    case 'LT':
                        return _this.stack.length < len;
                    case 'LE':
                        _this.stack.length <= len;
                    case 'GT':
                        return _this.stack.length > len;
                    case 'GE':
                        return _this.stack.length >= len;
                }
            else
                return _this.stack.length;
        };
        this.remove = function (type) {
            var idx = _this.stack.map(function (e) { return e.type; }).lastIndexOf(type);
            var elem = _this.stack[idx];
            _this.stack = _this.stack.filter(function (_, i) { return i != idx; });
            return elem;
        };
    }
    return Stack;
}());
exports.Stack = Stack;
function mapToType(str) {
    if (['[', ']'].includes(str))
        return 'SQUARE';
    else if (['(', ')'].includes(str))
        return 'NORMAL';
    else if (['{', '}'].includes(str))
        return 'CURLY';
    else if (['<', '>'].includes(str))
        return 'ANGLED';
    else
        throw Error("unknown Parenthesis type `" + str + "`");
}
exports.mapToType = mapToType;
function mapToCharacters(type) {
    switch (type) {
        case 'ANGLED':
            return ['<', '>'];
        case 'CURLY':
            return ['{', '}'];
        case 'NORMAL':
            return ['(', ')'];
        case 'SQUARE':
            return ['[', ']'];
    }
}
exports.mapToCharacters = mapToCharacters;
function opening(str) {
    if (['{', '(', '[', '<'].includes(str))
        return true;
    if (['}', ')', ']', '>'].includes(str))
        return false;
    else
        throw Error("unknown Parenthesis type `" + str + "`");
}
exports.opening = opening;
