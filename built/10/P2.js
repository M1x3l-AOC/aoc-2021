"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
var array = input_1.input.map(function (row) {
    return row.map(function (char) { return ({ type: util_1.mapToType(char), opening: util_1.opening(char) }); });
});
var incompleteRows = [];
for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
    var row = array_1[_i];
    var openingStack = new util_1.Stack();
    loop: {
        for (var _b = 0, row_1 = row; _b < row_1.length; _b++) {
            var currPar = row_1[_b];
            if (currPar.opening)
                openingStack.push(currPar);
            else if (currPar.type != ((_a = openingStack.pop()) === null || _a === void 0 ? void 0 : _a.type))
                break loop;
        }
        incompleteRows.push(row);
    }
}
var scores = [];
for (var _c = 0, incompleteRows_1 = incompleteRows; _c < incompleteRows_1.length; _c++) {
    var row = incompleteRows_1[_c];
    var stack = new util_1.Stack();
    for (var _d = 0, row_2 = row; _d < row_2.length; _d++) {
        var currPar = row_2[_d];
        if (currPar.opening)
            stack.push(currPar);
        else
            stack.remove(currPar.type);
    }
    var autocomplete = '';
    for (var i = 0; stack.length(); i++) {
        autocomplete += util_1.mapToCharacters(stack.pop().type)[1];
    }
    scores.push(autocomplete
        .split('')
        .map(function (e) { return util_1.TypeEnum[e]; })
        .reduce(function (acc, curr) { return acc * 5 + curr; }));
}
scores.sort(function (a, b) { return a - b; });
console.log(scores[scores.length >> 1]);
