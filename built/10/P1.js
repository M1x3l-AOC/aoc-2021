"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
var array = input_1.input.map(function (row) {
    return row.map(function (char) { return ({ type: util_1.mapToType(char), opening: util_1.opening(char) }); });
});
var errorScore = 0;
for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
    var row = array_1[_i];
    var currentErrorScore = 0;
    var stack = new util_1.Stack();
    charLoop: for (var _a = 0, row_1 = row; _a < row_1.length; _a++) {
        var currentParenthesis = row_1[_a];
        if (currentParenthesis.opening)
            stack.push(currentParenthesis);
        else {
            var topElement = stack.pop();
            if ((topElement === null || topElement === void 0 ? void 0 : topElement.type) != (currentParenthesis === null || currentParenthesis === void 0 ? void 0 : currentParenthesis.type)) {
                currentErrorScore +=
                    currentParenthesis.type == 'NORMAL'
                        ? 3
                        : currentParenthesis.type == 'SQUARE'
                            ? 57
                            : currentParenthesis.type == 'CURLY'
                                ? 1197
                                : 25137;
                break charLoop;
            }
        }
    }
    errorScore += currentErrorScore;
}
console.log(errorScore);
