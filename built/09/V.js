"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var string = '';
for (var _i = 0, input_2 = input_1.input; _i < input_2.length; _i++) {
    var row = input_2[_i];
    var rowStr = '';
    for (var _a = 0, row_1 = row; _a < row_1.length; _a++) {
        var char = row_1[_a];
        rowStr += colored(parseInt(char));
    }
    string += rowStr + "\n";
}
console.log(string);
function colored(num) {
    if (num == 9)
        return "\u001B[48;5;229m " + num + "\u001B[0m";
    return "\u001B[48;5;" + (236 + num * 2) + "m " + num + "\u001B[0m";
}
