"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var array = input_1.input.map(function (row) {
    return row.split('').map(function (cell) { return ({ num: parseInt(cell), checked: false }); });
});
var basins = [];
var _loop_1 = function (y) {
    var _loop_2 = function (x) {
        if (!getNeighbours(array, x, y).some(function (val) { return val.num < array[y][x].num; }))
            basins.push(traverseBasin(array, x, y));
    };
    for (var x = 0; x < array[0].length; x++) {
        _loop_2(x);
    }
};
for (var y = 0; y < array.length; y++) {
    _loop_1(y);
}
basins.sort(function (a, b) { return a - b; });
var largestBasins = [basins.pop(), basins.pop(), basins.pop()];
console.log(largestBasins.reduce(function (acc, curr) { return acc * curr; }));
///////////////
// Functions //
///////////////
function traverseBasin(array, x, y) {
    var currentSize = 1;
    if (array[y][x].checked)
        return 0;
    array[y][x].checked = true;
    if (array[y][x].num == 9)
        return 0;
    var neighbors = getNeighbours(array, x, y);
    for (var _i = 0, neighbors_1 = neighbors; _i < neighbors_1.length; _i++) {
        var _a = neighbors_1[_i], x_1 = _a.x, y_1 = _a.y;
        currentSize += traverseBasin(array, x_1, y_1) || 0;
    }
    return currentSize;
}
function getNeighbours(array, x, y) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    return [
        { x: x, y: y - 1, num: (_b = (_a = array[y - 1]) === null || _a === void 0 ? void 0 : _a[x]) === null || _b === void 0 ? void 0 : _b.num },
        { x: x - 1, y: y, num: (_d = (_c = array[y]) === null || _c === void 0 ? void 0 : _c[x - 1]) === null || _d === void 0 ? void 0 : _d.num },
        { x: x, y: y + 1, num: (_f = (_e = array[y + 1]) === null || _e === void 0 ? void 0 : _e[x]) === null || _f === void 0 ? void 0 : _f.num },
        { x: x + 1, y: y, num: (_h = (_g = array[y]) === null || _g === void 0 ? void 0 : _g[x + 1]) === null || _h === void 0 ? void 0 : _h.num },
    ].filter(function (elem) { return elem.num !== undefined; });
}
