"use strict";
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var array = input_1.input.map(function (row) { return row.split('').map(function (cell) { return parseInt(cell); }); });
var sum = 0;
var _loop_1 = function (y) {
    var _loop_2 = function (x) {
        if (![
            (_a = array[y - 1]) === null || _a === void 0 ? void 0 : _a[x],
            (_b = array[y]) === null || _b === void 0 ? void 0 : _b[x - 1],
            (_c = array[y + 1]) === null || _c === void 0 ? void 0 : _c[x],
            (_d = array[y]) === null || _d === void 0 ? void 0 : _d[x + 1],
        ].some(function (val) { return val <= array[y][x]; }))
            sum += array[y][x] + 1;
    };
    for (var x = 0; x < array[0].length; x++) {
        _loop_2(x);
    }
};
for (var y = 0; y < array.length; y++) {
    _loop_1(y);
}
console.log(sum);
