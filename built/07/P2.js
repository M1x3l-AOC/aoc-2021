"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var costs = [];
for (var i = Math.min.apply(Math, input_1.input); i < Math.max.apply(Math, input_1.input) + 1; i++) {
    var currCost = 0;
    for (var _i = 0, input_2 = input_1.input; _i < input_2.length; _i++) {
        var pos = input_2[_i];
        currCost += cost(Math.abs(pos - i));
    }
    costs.push(currCost);
}
function cost(n) {
    return Math.floor((1 / 2) * n * (n + 1));
}
console.log(Math.min.apply(Math, costs));
