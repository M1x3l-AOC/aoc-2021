"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var inp = __spreadArrays(input_1.example);
var _loop_1 = function (i) {
    var tmp = [];
    inp = inp.map(function (num) {
        if (num == 0) {
            tmp.push(8);
            return 6;
        }
        return num - 1;
    });
    inp.push.apply(inp, tmp);
};
for (var i = 0; i < 80; i++) {
    _loop_1(i);
}
console.log(inp.length);
