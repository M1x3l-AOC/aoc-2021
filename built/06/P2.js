"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var data = {
    0: input_1.input.filter(function (e) { return e == 0; }).length,
    1: input_1.input.filter(function (e) { return e == 1; }).length,
    2: input_1.input.filter(function (e) { return e == 2; }).length,
    3: input_1.input.filter(function (e) { return e == 3; }).length,
    4: input_1.input.filter(function (e) { return e == 4; }).length,
    5: input_1.input.filter(function (e) { return e == 5; }).length,
    6: input_1.input.filter(function (e) { return e == 6; }).length,
    7: input_1.input.filter(function (e) { return e == 7; }).length,
    8: input_1.input.filter(function (e) { return e == 8; }).length,
};
for (var i = 0; i < 256; i++) {
    var _a = __assign({}, data), n0 = _a[0], n1 = _a[1], n2 = _a[2], n3 = _a[3], n4 = _a[4], n5 = _a[5], n6 = _a[6], n7 = _a[7], n8 = _a[8];
    data[0] = n1;
    data[1] = n2;
    data[2] = n3;
    data[3] = n4;
    data[4] = n5;
    data[5] = n6;
    data[6] = n0 + n7;
    data[7] = n8;
    data[8] = n0;
}
var sum = 0;
for (var i = 0; i <= 8; i++) {
    sum += data[i];
}
console.log(sum);
