"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Board = /** @class */ (function () {
    function Board(str) {
        this.id = 0;
        this.board = str;
    }
    Board.prototype.at = function (row, col) {
        if (row > 5 || col > 5)
            throw Error('Array out of bounds');
        return this.board[row * 5 + col];
    };
    Board.prototype.check = function (row, col) {
        if (row > 5 || col > 5)
            throw Error('Array out of bounds');
        this.board[row * 5 + col].checked = true;
        return this;
    };
    Board.prototype.find = function (num) {
        for (var i = 0; i < this.board.length; i++) {
            var element = this.board[i];
            if (element.num == num)
                return [(i - (i % 5)) / 5, i % 5];
        }
        return null;
    };
    Board.prototype.hasWon = function () {
        var globBingo = false;
        loop: for (var i = 0; i < 5; i++) {
            var cells = [];
            for (var j = 0; j < 5; j++) {
                cells.push(this.at(i, j));
            }
            globBingo = cells.every(function (cell) { return cell.checked; });
            if (globBingo)
                break loop;
        }
        if (!globBingo)
            loop: for (var i = 0; i < 5; i++) {
                var cells = [];
                for (var j = 0; j < 5; j++) {
                    cells.push(this.at(j, i));
                }
                globBingo = cells.every(function (cell) { return cell.checked; });
                if (globBingo)
                    break loop;
            }
        if (!globBingo)
            return false;
        return [this.board.filter(function (cell) { return !cell.checked; }), this.id];
    };
    return Board;
}());
exports.Board = Board;
