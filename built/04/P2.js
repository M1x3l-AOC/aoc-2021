"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var wonBoardIDs = [];
var wonBoards = [];
for (var _i = 0, input_numbers_1 = input_1.input_numbers; _i < input_numbers_1.length; _i++) {
    var num = input_numbers_1[_i];
    var winBoard = [];
    for (var _a = 0, input_boards_1 = input_1.input_boards; _a < input_boards_1.length; _a++) {
        var board = input_boards_1[_a];
        var idx = board.find(num);
        if (idx)
            board.check.apply(board, idx);
        var _b = board.hasWon(), won = _b[0], id = _b[1];
        if (won && !wonBoardIDs.includes(id)) {
            winBoard = won;
            wonBoardIDs.push(id);
        }
    }
    if (winBoard.length) {
        wonBoards.push(winBoard.map(function (cell) { return cell.num; }).reduce(function (acc, curr) { return acc + curr; }) * num);
    }
}
console.log(wonBoards[wonBoards.length - 1]);
