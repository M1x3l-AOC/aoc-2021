"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var digits = '';
for (var j = 0; j < input_1.input[0].length; j++) {
    var n = [0, 0];
    for (var i = 0; i < input_1.input.length; i++) {
        n[parseInt(input_1.input[i][j])]++;
    }
    digits += n[0] > n[1] ? '0' : '1';
}
var gammaRate = parseInt(digits, 2);
var epsilonRate = parseInt(digits.replace(/0/g, '2').replace(/1/g, '0').replace(/2/g, '1'), 2);
console.log(gammaRate * epsilonRate);
