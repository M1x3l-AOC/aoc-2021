"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var util_1 = require("./DEPENDENCIES/util");
var range = util_1.parseInput(input_1.input);
var yPositions = [];
for (var x = 0; x < 1000; x++)
    for (var y = 0; y < 1000; y++) {
        var velocity = [x, y];
        var position = [0, 0];
        var maxY = 0;
        var hit = false;
        for (var i = 0; i < 1000 && !hit; i++) {
            position[0] += velocity[0];
            position[1] += velocity[1];
            velocity[0] += velocity[0] > 0 ? -1 : velocity[0] < 0 ? 1 : 0;
            velocity[1]--;
            maxY = Math.max(maxY, position[1]);
            hit = util_1.inRange(range, position);
        }
        if (hit)
            yPositions.push(maxY);
    }
console.log(Math.max.apply(Math, yPositions));
