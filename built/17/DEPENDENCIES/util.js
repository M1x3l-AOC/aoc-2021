"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseInput = function (str) {
    return str
        .match(/target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)/)
        .slice(1)
        .map(function (num) { return parseInt(num); });
};
function inRange(_a, _b) {
    var x1 = _a[0], x2 = _a[1], y1 = _a[2], y2 = _a[3];
    var x = _b[0], y = _b[1];
    return x1 <= x && x <= x2 && y1 <= y && y <= y2;
}
exports.inRange = inRange;
