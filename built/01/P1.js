"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var acc = 0;
var prev = Number.MAX_VALUE;
for (var _i = 0, input_2 = input_1.input; _i < input_2.length; _i++) {
    var num = input_2[_i];
    if (prev < num)
        acc++;
    prev = num;
}
console.log(acc);
