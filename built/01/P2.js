"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var sums = [];
for (var i = 0; i < input_1.input.length - 2; i++) {
    sums.push(input_1.input[i] + input_1.input[i + 1] + input_1.input[i + 2]);
}
var acc = 0;
var prev = Number.MAX_VALUE;
for (var _i = 0, sums_1 = sums; _i < sums_1.length; _i++) {
    var num = sums_1[_i];
    if (prev < num)
        acc++;
    prev = num;
}
console.log(acc);
