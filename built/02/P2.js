"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var aim = 0;
var pos = 0;
var depth = 0;
for (var _i = 0, input_2 = input_1.input; _i < input_2.length; _i++) {
    var str = input_2[_i];
    var _a = str.match(/(\w+) (\d+)/), _ = _a[0], direction = _a[1], amount = _a[2];
    switch (direction) {
        case 'forward':
            pos += parseInt(amount);
            depth += aim * parseInt(amount);
            break;
        case 'up':
            aim -= parseInt(amount);
            break;
        case 'down':
            aim += parseInt(amount);
            break;
    }
}
console.log(depth * pos);
