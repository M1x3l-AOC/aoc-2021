"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
var values = input_1.input
    .map(function (line) {
    return line.map(function (part) { return part.map(function (str) { return str.split('').sort().join(''); }); });
})
    .map(function (line) { return ({
    signalPatterns: line[0],
    outputValue: line[1],
}); });
var outputValues = [];
var _loop_1 = function (i) {
    var line = values[i];
    var unified = __spreadArrays(line.signalPatterns, line.outputValue);
    var map = [];
    map[1] = strSort(unified.filter(function (str) { return str.length == 2; })[0]);
    map[4] = strSort(unified.filter(function (str) { return str.length == 4; })[0]);
    map[7] = strSort(unified.filter(function (str) { return str.length == 3; })[0]);
    map[8] = strSort(unified.filter(function (str) { return str.length == 7; })[0]);
    var poss235 = unified
        .filter(function (str) { return str.length == 5; })
        .map(function (str) { return new Set(str); });
    var poss069 = unified
        .filter(function (str) { return str.length == 6; })
        .map(function (str) { return new Set(str); });
    var sets = [];
    sets[1] = new Set(map[1]);
    sets[4] = new Set(map[4]);
    map[6] = Array.from(poss069.find(function (elem) { return setIntersect(elem, sets[1]).length == 1; })).join('');
    var poss09 = poss069.filter(function (elem) { return Array.from(elem).join('') != map[6]; });
    map[9] = Array.from(poss09.find(function (elem) { return setIntersect(new Set(elem), sets[4]).length == 4; })).join('');
    map[0] = poss09
        .map(function (elem) { return Array.from(elem).join(''); })
        .filter(function (elem) { return elem != map[9]; })[0];
    map[3] = Array.from(poss235.find(function (elem) { return setIntersect(elem, sets[1]).length == 2; })).join('');
    var poss25 = poss235.filter(function (elem) { return Array.from(elem).join('') != map[3]; });
    map[2] = Array.from(poss25.find(function (elem) { return setIntersect(new Set(elem), sets[4]).length == 2; })).join('');
    map[5] = poss25
        .map(function (elem) { return Array.from(elem).join(''); })
        .filter(function (elem) { return elem != map[2]; })[0];
    var segmentMap = new Map(Array.from(map.map(function (elem) { return strSort(elem); }).entries()).map(function (elem) {
        return elem.reverse();
    }));
    var currentOutput = [];
    for (var _i = 0, _a = line.outputValue; _i < _a.length; _i++) {
        var val = _a[_i];
        currentOutput.push(segmentMap.get(val));
    }
    outputValues.push(parseInt(currentOutput.join('')));
};
for (var i = 0; i < values.length; i++) {
    _loop_1(i);
}
console.log(outputValues.reduce(function (acc, curr) { return acc + curr; }));
function strSort(str) {
    return str.split('').sort().join('');
}
function setIntersect() {
    var _a = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        _a[_i] = arguments[_i];
    }
    var set1 = _a[0], set2 = _a[1];
    return Array.from(set1).filter(function (str) { return set2.has(str); });
}
function setDifference() {
    var _a = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        _a[_i] = arguments[_i];
    }
    var set1 = _a[0], set2 = _a[1];
    return Array.from(set1).filter(function (str) { return !set2.has(str); });
}
