"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./DEPENDENCIES/input");
console.log(input_1.input
    .map(function (line) {
    return line[1].filter(function (str) {
        return str.length == 2 ||
            str.length == 4 ||
            str.length == 3 ||
            str.length == 7;
    }).length;
})
    .reduce(function (acc, curr) { return acc + curr; }));
